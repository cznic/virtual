module modernc.org/virtual

require (
	github.com/edsrzf/mmap-go v0.0.0-20170320065105-0bce6a688712
	github.com/remyoudompheng/bigfft v0.0.0-20170806203942-52369c62f446 // indirect
	golang.org/x/crypto v0.0.0-20181106171534-e4dc69e5b2fd
	golang.org/x/sys v0.0.0-20181107165924-66b7b1311ac8
	modernc.org/ccir v0.0.0-20181106174718-5753a3f77739
	modernc.org/golex v1.0.0 // indirect
	modernc.org/internal v1.0.0
	modernc.org/ir v1.0.0
	modernc.org/mathutil v1.0.0
	modernc.org/memory v1.0.0
	modernc.org/strutil v1.0.0 // indirect
	modernc.org/xc v1.0.0
)
